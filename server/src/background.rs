use std::time::Duration;

// Once every 2 hours
const BACKGROUND_JOB_INTERVAL: Duration = Duration::from_secs(60 * 60 * 2);

pub fn background_thread() {
    std::thread::Builder::new()
        .name("background-jobs".to_string())
        .spawn(move || {
            loop {
                std::thread::sleep(BACKGROUND_JOB_INTERVAL);

                // Cleanup auth database
                if let Err(error) = crate::auth::cleanup() {
                    log::error!("Error while running DB cleanup job: {error:?}");
                }
            }
        })
        .expect("Failed to start background job thread");
}

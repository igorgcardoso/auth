use crate::cache::TimedCache;
use argon2::{Error as HashError, Variant};
use auth_common::{AuthToken, ChangePasswordPayload};
use chrono::Utc;
use lazy_static::lazy_static;
use rusqlite::{params, Connection, Error as DbError, NO_PARAMS};
use serde_json::Error as JsonError;
use std::error::Error;
use std::fmt;
use std::{env, path::PathBuf};
use uuid::Uuid;

/// How long usernames are kept after an account is deleted (in days)
const USER_DELETED_NAME_KEEP: i64 = 30;

lazy_static! {
    static ref TOKENS: TimedCache = TimedCache::new();
}

fn apply_db_dir_override(db_dir: &str) -> String {
    if let Some(val) = env::var_os("AUTH_DB_DIR") {
        let path = PathBuf::from(val);
        if path.exists() || path.parent().map(|x| x.exists()).unwrap_or(false) {
            // Only allow paths with valid unicode characters
            if let Some(path) = path.to_str() {
                return path.to_owned();
            }
        }
        log::warn!("AUTH_DB_DIR is an invalid path.");
    }
    db_dir.to_string()
}

fn db() -> Result<Connection, AuthError> {
    let db_dir = &apply_db_dir_override("/opt/veloren-auth/data/auth.db");
    Ok(Connection::open(db_dir)?)
}

fn salt() -> [u8; 16] {
    rand::random::<u128>().to_le_bytes()
}

fn decapitalize(string: &str) -> String {
    string.chars().flat_map(char::to_lowercase).collect()
}

#[derive(Debug)]
pub enum AuthError {
    UserExists,
    UserDoesNotExist,
    UserRecentlyDeleted,
    InvalidLogin,
    InvalidToken,
    Db(DbError),
    Hash(HashError),
    Json(JsonError),
    InvalidRequest(String),
    RateLimit,
}

impl AuthError {
    pub fn status_code(&self) -> u16 {
        match self {
            Self::UserExists => 400,
            Self::UserDoesNotExist => 400,
            Self::UserRecentlyDeleted => 400,
            Self::InvalidLogin => 400,
            Self::InvalidToken => 400,
            Self::Db(_) => 500,
            Self::Hash(_) => 500,
            Self::Json(_) => 400,
            Self::InvalidRequest(_) => 400,
            Self::RateLimit => 429,
        }
    }
}

impl fmt::Display for AuthError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::UserExists => "That username is already taken.".into(),
                Self::UserDoesNotExist => "That user does not exist.".into(),
                Self::UserRecentlyDeleted => "This account was recently deleted".into(),
                Self::InvalidLogin =>
                    "The username + password combination was incorrect or the account does not exist."
                        .into(),
                Self::InvalidToken => "The given token is invalid.".into(),
                Self::Db(err) => format!("Database error: {}", err),
                Self::Hash(err) => format!("Error securely storing password: {}", err),
                Self::Json(err) => format!("Error decoding JSON: {}", err),
                Self::InvalidRequest(s) =>
                    format!("The request was invalid in some form. Reason: {}", s),
                Self::RateLimit => "You are sending too many requests. Please slow down.".into(),
            }
        )
    }
}

impl Error for AuthError {}

impl From<DbError> for AuthError {
    fn from(err: DbError) -> Self {
        Self::Db(err)
    }
}

impl From<HashError> for AuthError {
    fn from(err: HashError) -> Self {
        Self::Hash(err)
    }
}

impl From<JsonError> for AuthError {
    fn from(err: JsonError) -> Self {
        Self::Json(err)
    }
}

pub fn init_db() -> Result<(), AuthError> {
    db()?.execute(
        "
        CREATE TABLE IF NOT EXISTS users (
            uuid TEXT NOT NULL PRIMARY KEY,
            username TEXT NOT NULL UNIQUE,
            display_username TEXT NOT NULL UNIQUE,
            pwhash TEXT NOT NULL
        );
    ",
        NO_PARAMS,
    )?;
    db()?.execute(
        "
        CREATE TABLE IF NOT EXISTS deleted_users (
            username TEXT PRIMARY KEY,
            deleted_at INT NOT NULL
        );
    ",
        NO_PARAMS,
    )?;
    Ok(())
}

/// Checks whether an username has been recently deleted
fn username_reserved(username: &str) -> Result<bool, AuthError> {
    Ok(db()?
        .prepare("SELECT 1 FROM deleted_users WHERE username == ?1")?
        .exists(params![username])?)
}

fn user_exists(username: &str) -> Result<bool, AuthError> {
    let db = db()?;
    let mut stmt = db.prepare("SELECT uuid FROM users WHERE username == ?1")?;
    Ok(stmt.exists(params![username])?)
}

pub fn cleanup() -> Result<(), AuthError> {
    let too_old = Utc::now() - chrono::Duration::days(USER_DELETED_NAME_KEEP);

    db()?.execute(
        "DELETE FROM deleted_users WHERE deleted_at <= ?1",
        params![too_old.timestamp()],
    )?;

    Ok(())
}

#[allow(clippy::let_and_return)]
pub fn username_to_uuid(username_unfiltered: &str) -> Result<Uuid, AuthError> {
    let username = decapitalize(username_unfiltered);
    let db = db()?;
    let mut stmt = db.prepare_cached("SELECT uuid FROM users WHERE username == ?1")?;
    let result = stmt
        .query_map(params![&username], |row| row.get::<_, String>(0))?
        .filter_map(|s| s.ok())
        .find_map(|s| Uuid::parse_str(&s).ok())
        .ok_or(AuthError::UserDoesNotExist);
    result
}

#[allow(clippy::let_and_return)]
pub fn uuid_to_username(uuid: &Uuid) -> Result<String, AuthError> {
    let db = db()?;
    let uuid = uuid.as_simple().to_string();
    let mut stmt = db.prepare_cached("SELECT display_username FROM users WHERE uuid == ?1")?;
    let result = stmt
        .query_map(params![uuid], |row| row.get::<_, String>(0))?
        .find_map(|s| s.ok())
        .ok_or(AuthError::UserDoesNotExist);
    result
}

pub fn register(username_unfiltered: &str, password: &str) -> Result<(), AuthError> {
    let username = decapitalize(username_unfiltered);
    if user_exists(&username)? {
        return Err(AuthError::UserExists);
    }

    if username_reserved(&username)? {
        return Err(AuthError::UserRecentlyDeleted);
    }

    let uuid = Uuid::new_v4().as_simple().to_string();
    let pwhash = hash_password(password)?;
    db()?.execute(
        "INSERT INTO users (uuid, username, display_username, pwhash) VALUES(?1, ?2, ?3, ?4)",
        params![uuid, &username, username_unfiltered, pwhash],
    )?;
    Ok(())
}

pub fn change_password(
    ChangePasswordPayload {
        username,
        current_password,
        new_password,
    }: ChangePasswordPayload,
) -> Result<(), AuthError> {
    let username = username.to_lowercase();

    if !is_valid(&username, &current_password)? {
        Err(AuthError::InvalidLogin)?
    }

    let uuid = username_to_uuid(&username)?.as_simple().to_string();
    let pwhash = hash_password(&new_password)?;

    db()?.execute(
        "UPDATE users SET pwhash = ?1 WHERE uuid = ?2",
        params![pwhash, uuid],
    )?;
    Ok(())
}

/// Checks if the password is correct and that the user exists.
#[allow(clippy::let_and_return)]
fn is_valid(username: &str, password: &str) -> Result<bool, AuthError> {
    let db = db()?;
    let mut stmt = db.prepare_cached("SELECT pwhash FROM users WHERE username == ?1")?;
    let result = stmt
        .query_map(params![&username], |row| row.get::<_, String>(0))?
        .filter_map(|s| s.ok())
        .find_map(|correct| argon2::verify_encoded(&correct, password.as_bytes()).ok())
        .ok_or(AuthError::InvalidLogin);
    result
}

/// Hashes a password using Argon2
fn hash_password(password: &str) -> Result<String, AuthError> {
    let hconfig = argon2::Config {
        variant: Variant::Argon2i,
        time_cost: 3,
        mem_cost: 4096,
        ..Default::default()
    };
    Ok(argon2::hash_encoded(
        password.as_bytes(),
        &salt(),
        &hconfig,
    )?)
}

pub fn generate_token(username_unfiltered: &str, password: &str) -> Result<AuthToken, AuthError> {
    let username = decapitalize(username_unfiltered);
    if !is_valid(&username, password)? {
        return Err(AuthError::InvalidLogin);
    }

    let uuid = username_to_uuid(&username)?;
    let token = AuthToken::generate();
    TOKENS.insert(token, uuid);
    Ok(token)
}

pub fn delete_account(username_unfiltered: &str, password: &str) -> Result<(), AuthError> {
    let username = decapitalize(username_unfiltered);
    if !is_valid(&username, password)? {
        return Err(AuthError::InvalidLogin);
    }
    let uuid = username_to_uuid(&username)?.as_simple().to_string();
    let db = db()?;
    db.execute(
        "DELETE FROM users WHERE uuid = ?1 AND username = ?2",
        params![uuid, &username],
    )?;

    let now = Utc::now().timestamp();
    db.execute(
        "INSERT INTO deleted_users VALUES (?1, ?2)",
        params![username, now],
    )?;

    Ok(())
}

pub fn verify(token: AuthToken) -> Result<Uuid, AuthError> {
    let mut uuid = None;
    TOKENS.run(&token, |entry| {
        uuid = entry.map(|e| e.data);
        false
    });
    uuid.ok_or(AuthError::InvalidToken)
}
